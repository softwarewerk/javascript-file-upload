( function( $ ) {
    var fileUpload = {
        init: function () {
            var $successContainer = $('[data-file-upload-valid]');
            var $errorContainer = $('[data-file-upload-error]');
            $(document).on('change', '[data-file-upload-input]', function() {
                var filePath =  $(this).val();
                var fileName = filePath.split('\\').pop();
                var validFileType = $(this).attr('data-file-upload-input');

                // check if filename has the desired extension
                var extension = filePath.split('.').pop();
                if ((validFileType !== undefined && extension === validFileType) || (validFileType === undefined)) {
                    $successContainer.show();
                    $errorContainer.hide();
                    $('[data-file-upload-name]').text(fileName);
                } else {
                    $successContainer.hide();
                    $('[data-file-upload-error]').show();
                    return false;
                }
            });

            $(document).on('click', '[data-file-upload-reset]', function() {
                $errorContainer.hide();
                $successContainer.hide();
            });
        }
    };
} )( jQuery );
